package hello;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;
import javax.management.openmbean.OpenMBeanInfo;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import com.sun.jdmk.comm.HtmlAdaptorServer;

public class HelloAgent2 {
	public static void main(String args[]) throws Exception {
//		MBeanServer server = MBeanServerFactory.createMBeanServer();
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		
		ObjectName helloName = new ObjectName("chengang:name=HelloWorld");
		Hello hello = new Hello();
		server.registerMBean(hello, helloName);
		ObjectName adapterName = new ObjectName(
				"HelloAgent:name=htmladapter,port=8082");
		//HtmlAdaptorServer adapter = new HtmlAdaptorServer();
		//server.registerMBean(adapter, adapterName);
		//adapter.start();
		System.out.println("start.....");
		System.out.println(HtmlAdaptorServer.class.isAssignableFrom(OpenMBeanInfo.class));

		// Create an RMI connector and start it
		// JMXServiceURL url = new
		// JMXServiceURL("service:jmx:rmi:///jndi/rmi://127.0.0.1:9999/server");
		// JMXConnectorServer cs =
		// JMXConnectorServerFactory.newJMXConnectorServer(url, null, server);
		// cs.start();

		System.out.println("rmiĦĦstart.....");
	}
}
